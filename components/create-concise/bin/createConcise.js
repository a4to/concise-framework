#!/usr/bin/env node

const {execSync} = require('child_process');
const path = require('path');
const fs = require('fs');

function appName() { 
  if (process.argv[2] == undefined) {
    return path.basename(process.cwd());
  } else {
    return process.argv[2];
  }
}

const runCmd =  (cmd) => {
  try {
    execSync(cmd, {stdio: 'inherit'});
  } catch (e) {
    console.error(`Error - Failed to execute: ${cmd}`, e);
    return false;
  }
  return true;
}

const installDepsCmd = `cd ${appName()} && npx yarn`;
const startConcise = `cd ${appName()} && npx yarn start`;
const gitCheckoutCmd = `git clone --depth 1 -b base https://gitlab.com/a4to/concise-framework.git ${appName()}`;

function getLatestVersion(app) {
  const version = execSync(`npm show ${app} version`).toString().trim();
  return version;
}

const reactVersion = getLatestVersion('react');
const react_domVersion = getLatestVersion('react-dom');
const react_scriptsVersion = getLatestVersion('react-scripts');
const web_vitalsVersion = getLatestVersion('web-vitals');



const packageJson = `{
  "name": "${appName()}",
  "version": "0.1.0",
  "description": "",
  "license": "MIT",
  "main": "src/app.js",
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "serve": "react-scripts build && serve -s build"
  },
  "dependencies": {
    "react": "^${reactVersion}",
    "react-dom": "^${react_domVersion}",
    "react-scripts": "^${react_scriptsVersion}",
    "web-vitals": "^${web_vitalsVersion}"
  },
  "browserslist": {
    "production": [
      ">0.2%",
      "not dead",
      "not op_mini all"
    ],
    "development": [
      "last 1 chrome version",
      "last 1 firefox version",
      "last 1 safari version"
    ]
  }
}`;

const log = (color, text) => {
  console.log(`${color}%s${Log.rst}`, text);
};

const Log = {
  fg: { blk: "\x1b[30m", rd: "\x1b[31m",  grn: "\x1b[32m", 
        yel: "\x1b[33m", bl: "\x1b[34m",  mag: "\x1b[35m", 
        cyn: "\x1b[36m", fff: "\x1b[37m",crim: "\x1b[38m" 
      },
  rst: "\x1b[0m", bri: "\x1b[1m",
  dim: "\x1b[2m", unscore: "\x1b[4m",
  bnk: "\x1b[5m", rev: "\x1b[7m",
  hide: "\x1b[8m"
};


log(Log.fg.grn, `[+] Creating Concise Application Base ...` + Log.rst);
const checkedOut = runCmd(gitCheckoutCmd);
const makePackageJson = fs.writeFileSync(`${appName()}/package.json`, packageJson);
if (!checkedOut || !packageJson) process.exit(1);

log(Log.fg.yel, `[*] Installing dependencies ...` + Log.rst);
const installDeps = runCmd(installDepsCmd);
if (!installDeps) process.exit(1);

log(Log.fg.grn, `[+] Successfully Bootstrapped ${appName()} into ${appName()}, Congratulations!` + Log.rst);
log(Log.fg.mag, `Beginning Deployment...` + Log.rst);
const started = runCmd(startConcise);
if (!started) process.exit(1);

log(Log.fg.grn, `Thank you for using Concise!` + Log.rst);


