import React from 'react';
import ReactDOM from 'react-dom/client';
import './css/style.css';
import Base from './base.js';
import reportWebVitals from './js/reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Base />
  </React.StrictMode>
);

reportWebVitals()
