import logo from "./img/logo.svg";
import logoName from "./img/logoName.png";
import "./css/style.css";

const logoStyle = {
 color: '#0f0'
};

function Base() {
  return (
    <div className="Base">
      <header className="Base-header">
        <img src={logo} className="Base-logo" alt="" /><br/>
        <img src={logoName} className="Base-logoName" alt="" />
       <title2><p>
        Web Development Framework
        </p></title2>
      </header>
    </div>
  );
}

export default Base
